// definisce cosa pubblicare al client
//*********************************************************

Meteor.publish("wikis", function (userUUID, roundId) {
	
	//console.log("wiki userUUID not equals: " + userUUID + " -- roundId: " + roundId);

	// recupero la wiki del player per assicurarmi che lo userUUID in input sia associato a qualcuno della partita..
	// altrimenti si potrebbe da client ottenerli tutti
	var player = Players.findOne({userUUID: userUUID});
	if (!player) {
		console.log('userUUID inesistente!');
		return [];
	}

	// var round = Rounds.findOne({party: player.party, status: { $ne: 'finished' } } );
	// if (!round) {
	// 	return [];
	// }

  	return Wikis.find(
	  	{
			round: roundId
		},
		{ 
			$or: [ 
					{ player: {$ne: player._id} },  			// la wiki non è del giocatore
					{ player:  player._id, status: 'guessed' }, // oppure è del giocatore ed è già stata indovinata
					{ player:  player._id, status: 'gaveup' }   // oppure è del giocatore e si è arreso
				] 
		}
	);
});

Meteor.publish("parties", function (partyId) {
  	return Parties.find(partyId);
});

Meteor.publish("rounds", function (partyId) {
  	return Rounds.find({party: partyId});
});

Meteor.publish("players", function (partyId) {
  	return Players.find({party: partyId}, {fields: {userUUID: 0}});
});