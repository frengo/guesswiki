// definisce le route applicative, ossia quale layout mostrare 
// a seconda del context specificato
//*********************************************************

Router.configure({
    layoutTemplate: 'layout' // here we say that layout template will be our main layout
});


Router.route('/', {
	template: 'start'
});

Router.route('/play', {
	template: 'start'
});


Router.route('/info', {
	template: 'info'
});

