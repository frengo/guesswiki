Template.wikiItem.helpers({
	'isPending': function() {
		console.log(this);
		if (this || this.status) {
			return this.status === 'pending';
		} else {
			return false;
		}
	}
});

Template.wikiItem.events({
  'click #wiki-approve': function(event, template) {

		console.log('Approving wiki..');
  		
  		var wikiId = this._id;
  		var playerId = Session.get('playerId');

  		console.log('wiki id: ' + wikiId);
  		console.log('player id: ' + playerId);

  		Meteor.call('approveWiki', {wikiId: wikiId, playerId: playerId}, function(err, resp) {
  			if (err) {
  				console.log(err);
  				alert(err.details);
  				return;
  			};
  		});
   },


  'click #wiki-disapprove': function(event, template) {

		console.log('Disapproving wiki..');
  		
  		var wikiId = this._id;
  		var playerId = Session.get('playerId');

  		console.log('wiki id: ' + wikiId);
  		console.log('player id: ' + playerId);

  		Meteor.call('disapproveWiki', {wikiId: wikiId, playerId: playerId}, function(err, resp) {
  			if (err) {
  				console.log(err);
  				alert(err.details);
  				return;
  			};
  		});
   }
});