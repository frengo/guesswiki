Template.start.helpers({


	'itsMe': function() {
		return this._id == Session.get("playerId");
	},

	'amIMaster': function() {
		var playerId = Session.get('playerId');
		var player = Players.findOne(playerId);
		if (player) {
			return player.isMaster;
		} else {
			return false;
		}
	}

});

Template.start.events({
  'click #create': function(event, template) {
    
	console.log("create");

    var playerName = template.find("#playerName").value;
    console.log("Player name: " + playerName);

    console.log("Calling method create new game..");
	Meteor.call('create', {playerName: playerName}, function(err, response) {

		if (err) {
			console.log(err);
			alert(err.details);
			return;
		};

		console.log("Data from method create..");

		Session.set("partyId", response.partyId);
		Session.set("playerId", response.playerId);
		Session.set("master", true);
		Session.set("userUUID", response.userUUID);

		if (localStorage) {
			localStorage.setItem("userUUID", Session.get("userUUID"));
			localStorage.setItem("partyId", Session.get("partyId"));
			localStorage.setItem("playerId", Session.get("playerId"));
		}
		// Meteor.subscribe('wikis', response.userUUID);
	});

	console.log("party, round and player saved!");

  },
/*------------------------------------------------------*/
/*------------------------------------------------------*/
  'click #join': function(event, template) {
    console.log("join");

	var playerName = template.find("#playerName").value;
	var joinCode = template.find("#joinCode").value;

	if (!playerName) {
		alert("Please set player name");
		return;
	}


	if (!joinCode) {
		alert("Please set player name");
		return;
	}

	console.log("Calling method join..");
	Meteor.call('join', {playerName: playerName, joinCode: joinCode}, function(err, response) {
		
		if (err) {
			console.log(err);
			alert(err.details);
			return;
		};

		console.log("Data from method join..");

		Session.set("partyId", response.partyId);
		Session.set("playerId", response.playerId);
		Session.set("userUUID", response.userUUID);

		if (localStorage) {
			localStorage.setItem("userUUID", Session.get("userUUID"));
			localStorage.setItem("partyId", Session.get("partyId"));
			localStorage.setItem("playerId", Session.get("playerId"));
		}

		// Meteor.subscribe('wikis', response.userUUID);
	});

  },
/*------------------------------------------------------*/
/*------------------------------------------------------*/
  'click #reset': function() {
	var userUUID = Session.get('userUUID');

  	Meteor.call('exit', {userUUID: userUUID}, function(err, response) {
		
		if (err) {
			console.log(err);
			alert(err.details);
		};

		Session.set("partyId", 0);
		Session.set("roundId", 0);
		Session.set("playerId", 0);
		Session.set("master", false);
		Session.set("userUUID", 0);

		if (localStorage) {
			localStorage.setItem("userUUID", Session.get("userUUID"));
			localStorage.setItem("partyId", Session.get("partyId"));
			localStorage.setItem("playerId", Session.get("playerId"));
		}

	  	if (timer) {
	  		clearInterval(timer);
	  	}		
  	});
  },
/*------------------------------------------------------*/
/*------------------------------------------------------*/
  'click .begin': function(event, template) {

  	console.log("Calling method begin..");
	Meteor.call('begin', {partyId: Session.get('partyId'), playerId: Session.get('playerId')}, function(err, response) {
		if (err) {
			console.log(err);
			alert(err.details);
		};
	});
  },
/*------------------------------------------------------*/
/*------------------------------------------------------*/
  'click .start': function(event, template) {

  	console.log("Calling method start..");
  	var round = Rounds.findOne({party: Session.get('partyId'), status: { $nin: ['finished'] } } );
	Meteor.call('start', {roundId: round._id}, function(err, response) {
		if (err) {
			console.log(err);
			alert(err.details);
			return;
		};
	});
  },
/*------------------------------------------------------*/
/*------------------------------------------------------*/
  'click .next': function(event, template) {

  	console.log("Calling method next..");
  	var round = Rounds.findOne({party: Session.get('partyId'), status: { $nin: ['finished'] } } );
	Meteor.call('next', {roundId: round._id}, function(err, response) {
		if (err) {
			console.log(err);
			alert(err.details);
			return;
		};
	});
  },

/*------------------------------------------------------*/
/*------------------------------------------------------*/
  'click .solve': function(event, template) {

	var solution = template.find("#solution").value;

  	console.log("Calling method solve. Solution: " + solution);
	Meteor.call('solve', {userUUID: Session.get('userUUID'), solution: solution}, function(err, response) {
		if (err) {
			console.log(err);
			alert(err.details);
			return;
		};
		alert("Complimenti!");

	});
  },

/*------------------------------------------------------*/
/*------------------------------------------------------*/
  'click .giveUp': function(event, template) {

  	console.log("Calling method giveUp.. ");
	Meteor.call('solve', {userUUID: Session.get('userUUID'), giveUp: 'true'}, function(err, response) {
		if (err) {
			console.log(err);
			alert(err.details);
			return;
		};
		alert("Peccato!");

	});
  }    
});





