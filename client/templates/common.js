if (localStorage) {
	
	Session.set("userUUID", localStorage.getItem('userUUID'));
	Session.set("playerId", localStorage.getItem('playerId'));
	Session.set("partyId", localStorage.getItem('partyId'));
}



Template.registerHelper('eq', function (a, b) {
	return a === b;
});
Template.registerHelper('ne', function (a, b) {
	return a !== b;
});

Template.registerHelper('playerId', function() {
	return Session.get("playerId");
});

Template.registerHelper('party', function() {
	console.log("getting party..");	
	return Parties.findOne(Session.get("partyId"));
});	


Template.registerHelper('roundSecondsLeft', function() {
	return Session.get('roundSecondsLeft');
});	


Template.registerHelper('round', function() {
	//console.log("getting round..");
	var round = Rounds.findOne({party: Session.get('partyId'), status: { $ne: 'finished' } } );
	if (round) {
		Session.set("roundId", round._id);
		return round;
	} else {
		console.log("No active round found!");
		return {
			status: 'finished'
		};
	}
});	

Template.registerHelper('hasSolvedTheWiki', function() {

	console.log("has the player solved  the wiki?");
	var round = Rounds.findOne({party: Session.get('partyId'), status: { $ne: 'finished' } } );
	return GTW.contains(round.solvers, Session.get('playerId'));

});



var subscribe = function () {

	console.log('**** subscribe ****');

    var partyId = Session.get('partyId');
    var userUUID = Session.get('userUUID');

    var party = Parties.findOne(partyId);
	
    if (party) {
		console.log('Subscribing for data party ' + partyId);

		Meteor.subscribe("parties", partyId);
		Meteor.subscribe("rounds", partyId);
		Meteor.subscribe("players", partyId);
		if (party.round) { 
			Meteor.subscribe("wikis", userUUID, party.round);
		}    	
    }


};



var timerClient;
Tracker.autorun(function () {

  var partyId = Session.get('partyId');
  var userUUID = Session.get('userUUID');
  var roundId = Session.get('roundId');


  var round = Rounds.findOne({party: Session.get('partyId'), status: { $nin: ['finished'] } } );
  console.log("Round changed!");
  
  subscribe();

  if (!round) {
  	return;
  }

  if (round.status === 'started') {
  	console.log("Round player: " + round.player);
  	
  	if (timerClient) {
  		clearInterval(timerClient);
  	}


  	timerClient = Meteor.setInterval(function() {

		Meteor.call('roundSecondsLeft', {round: round, userUUID: userUUID}, function(err, secondsLeft) {

			if (secondsLeft < 0) {
				console.log("time's up");
				Session.set('roundSecondsLeft', 0);
				clearInterval(timerClient);
				return;
			}

			Session.set('roundSecondsLeft', Math.round(secondsLeft));

		});
		

  	}, 500);


  } else {
  	console.log('Round not started');
  	if (timerClient) {
		Meteor.clearInterval(timerClient);
  		Session.set('roundSecondsLeft', 0);
  	}

  }

  return;

});



Tracker.autorun(function() {

	console.log('**** autorun ****');

    var partyId = Session.get('partyId');
    console.log('partyId: ' + partyId);
	
	Meteor.subscribe("parties", partyId);

    var userUUID = Session.get('userUUID');

    var party = Parties.findOne(partyId);
	console.log('party: ' + party);

    if (party) {
		console.log('Subscribing for data party ' + partyId);

		Meteor.subscribe("rounds", partyId);
		Meteor.subscribe("players", partyId);
		if (party.round) { 
			Meteor.subscribe("wikis", userUUID, party.round);
		}    	
    }


});
