Template.playersList.helpers({

	'players': function() {
		console.log("getting players..");
		return Players.find({party: Session.get("partyId") }, {sort: {score: -1, _id: 1} } );
	},


	'playerWiki': function(playerId) {
		
		var round = Rounds.findOne({party: Session.get('partyId'), status: { $ne: 'finished' } } );
		console.log("getting wikis. PlayerID: " + playerId + ' -- RoundID: ' + round._id);
		var wikis = Wikis.findOne({player: playerId, round: round._id});

		return wikis;
	}
});




