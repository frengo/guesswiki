Parties = new Mongo.Collection('party');
/*
party.status = [open, closed]
*/

Players = new Mongo.Collection('players');


Rounds = new Mongo.Collection('round');
/*
round.status = [pending, begun, ready, started, cancelled, finished]
*/

Wikis = new Mongo.Collection('wiki');
/*
wiki.status = [pending, approved, guessed, gaveup]
*/


// Calcola il prossimo codice di 4 cifre da restituire
Parties.nextCode = function() {
  
  var nextCode = "";

  do {
  	var one = Math.floor(Math.random() * 10);
  	var two = Math.floor(Math.random() * 10);
  	var three = Math.floor(Math.random() * 10);
  	var four = Math.floor(Math.random() * 10);
  	nextCode = one+""+two+""+three+""+four;

  } while(Parties.findOne({joinCode: nextCode}));


  return nextCode;
};

// inserimento di un nuovo player
Players.insertWithDefaults = function(partyId, playerName, isMaster, userUUID) {

    return Players.insert({
        party: partyId,
        name: playerName, 
        isMaster: isMaster,
        roundCount: 0,
        userUUID: userUUID,
        date: new Date() 
    });	
};

Wikis.vote = function(wikiId, playerId, vote) {

    var wiki = Wikis.findOne(wikiId);

    if (!wiki) {
        console.log("No wiki found, id: " + wikiId);
        throw new Meteor.Error(500, "No wiki found", "No wiki found");
    }

    // prendo il player che ha la wiki
    var wikiPlayerId = wiki.player;


    console.log("Wiki status: " + wiki.status);

    if (wiki.status != 'pending') {
        console.log("Wiki status is not pending");
        throw new Meteor.Error(500, "Wiki status is not pending", "Wiki status is not pending");
    }

    var round = Rounds.findOne(wiki.round);
    console.log("Round status: " + round.status);
    // il round dev'essere iniziato..
    if (!round || round.status != 'begun') {
        console.log("Round has not begun");
        throw new Meteor.Error(500, "Round has not begun", "Round has not begun");
    }    

    var party = Parties.findOne(round.party);

    console.log("Party status: " + party.status);

    // la partita deve essere chiusa.. nessun altro giocatore può entrare
    if (party.status != 'closed') {
        console.log("Party not closed yet");
        throw new Meteor.Error(500, "Party not closed yet", "Party not closed yet");
    }



    // recupero elenco giocatori della partita
    // var players = Players.find({party: wiki.party}).fetch();
    console.log("Number of players: " + round.playersCount);

    var pros = wiki.pros ? wiki.pros : [];
    var cons = wiki.cons ? wiki.cons : [];

    if (GTW.contains(pros,playerId) || GTW.contains(cons, playerId)) {
        console.log("Player already voted");
        throw new Meteor.Error(500, "Player already voted", "Player already voted");
    }

    if (vote == 'approve') {
        // aggiungo il player all'array dei votanti 'pro'
        console.log("Voting for approve..");
        Wikis.update(wiki._id, {$push: {pros: playerId}} );
        wiki = Wikis.findOne(wiki._id);
        console.log("Wiki pros: " + wiki.pros.length);

        if (wiki.pros.length > (round.playersCount/2-1) ) {
            // abbiamo raggiunto il quorom.. wiki approvata!
            console.log("Wiki approved :)");

            Wikis.update(wiki._id, {$set: {status: 'approved'}});



            // se anche tutte le altre wiki sono approvate allora inizia il round!!
            var allPartyWiki = Wikis.find({round: wiki.round});
            var allApproved = true;
            allPartyWiki.forEach(function(item){
                if (item.status !== 'approved') {
                    allApproved = false;
                }
            });


            if (allApproved) {
                console.log("All wiki approved!");
                // aggiorno lo stato del round a started
                Rounds.update(round._id, {$set: {status: 'ready'}} );
            }

        }

    } else {
        console.log("Voting for disapprove..");
        Wikis.update(wiki._id, {$push: {cons: playerId}} );
        wiki = Wikis.findOne(wiki._id);
        console.log("Wiki cons: " + wiki.cons.length);

        if (wiki.cons.length > (round.playersCount/2-1) ) {
            // abbiamo raggiunto il quorom.. wiki NON approvata!
            console.log("Wiki NOT approved :(");

            Wikis.remove(wiki._id);

            console.log("Inserting a new wiki");
            Wikis.retrieveAndInsertWithDefaults(round._id, wikiPlayerId);
        }        
    }

};

// inserisce una nuova wiki, recuperando la parola da indovinare nel file dizionario
Wikis.retrieveAndInsertWithDefaults = function(roundId, playerId) {

    
    if(Meteor.isServer){

        console.log("Recupero della wiki da indovinare..");
        
        var wiki = GTW.getDictionaryWord('it');

        console.log("Wiki recuperata:");
        console.log(wiki);


        var wikiId =  Wikis.insert({
            url: wiki.url,
            title: wiki.title,
            round: roundId,
            player: playerId,
            status: 'pending'
        });            

        console.log('Wiki saved. ID: ' + wikiId);
        
        return wikiId;    

    }

};


