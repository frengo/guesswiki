/* 
	Libreria di progetto che contiene le principali utilities generiche
*/
function _GTW() {
    return this;
}



_GTW.prototype = {

/*
 * Funzione che restituisce true se a è contenuta nell'array obj.
 * obj può anche essere una stringa o comunque un oggetto iterabile
 *  
 */    
    contains: function(a, obj) {
        if (!a) return false;
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    },

/*
 * Funzione che rimuove gli spazi a destra e sinistra della stringa passata in input
 */
    trim: function(s) {

        return s.replace(/^\s+|\s+$/gm,'');
    },

/**
 * Funzione che scorre il file contenente tutte le voci del dizionario italiano
 * recuperando una parola a caso.
 * Restituisce un oggetto contenente title=parola del dizionario e url=pagina web con il dettaglio della voce
 *
 */
    getDictionaryWord: function(lang) {

        var wikiUrl;
        var wikiTitle;

        wikiUrl = '#';
        wikiTitle = ''

        if (Meteor.isServer) {

            var workingDir = process.cwd();
            var fullPath = workingDir + '/assets/app/dictionary_'+lang+'.txt';
            console.log(fullPath);

            // var stream = fs.readFileSync(fullPath);
            var data = fs.readFileSync(fullPath, 'utf-8');
            var lines = data.split('\n');
            wikiTitle = lines[Math.floor(Math.random()*lines.length)];
            wikiTitle = wikiTitle.replace(/\r/g, '');

            
            if (lang === 'it') {
                wikiUrl = 'http://www.dizi.it/'+wikiTitle;  
            } else {
                wikiUrl = 'http://www.google.com?q=' + wikiTitle + '&hl=' + lang;
            }

        } 

        /****************************
        // // get wiki
         var url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D'https%3A%2F%2Fit.wikipedia.org%2Fwiki%2FSpeciale%3APaginaCasuale'%20and%20xpath%3D'%2F%2Fh1'&format=json&diagnostics=true&callback=";
         var results = Meteor.http.get(url);
         var output = JSON.parse(results.content);
         wikiUrl = output.query.diagnostics.redirect.content;
         wikiTitle = output.query.results.h1.content;
        ****************************/

        return {url: wikiUrl, title: wikiTitle};	
    }
}


GTW = new _GTW();