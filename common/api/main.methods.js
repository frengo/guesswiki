// rappresenta il timer per ciascun round, ossia tiene il tempo di ciascun round di gioco
timers = {};

Meteor.methods({

    
    // Inizia una nuova partita
	'create': methodCreate,

    // permette di unirsi ad una partita, indicando il codice di ingresso
    'join': methodJoin,

    // Close the party and begin the first round
    'begin': methodBegin,

    // permette al giocatore di lasciare il gioco
    'exit': methodExit,

    // approva la wiki di un altro giocatore
    'approveWiki': methodApproveWiki,

    // disapprova la wiki di un altro giocatore
    'disapproveWiki': methodDisapproveWiki,

    // inizia un nuovo gioco
    'start': methodStart,    

    // muove il turno al prossimo giocatore
    'next': methodNext,

    // permette di indovinare la wiki
    'solve': methodSolve,

    // restituisce il numero di secondi mancanti prima della fine del turno del giocatore
    'roundSecondsLeft': methodSecondsLeft

});




