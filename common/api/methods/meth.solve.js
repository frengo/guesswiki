methodSolve = function(data) {

        console.log("Meteor method --- solve");

        var userUUID = data.userUUID;
        var solution = data.solution;

        if (solution) {
            solution = new String(solution);
            solution = GTW.trim(solution);
            solution = solution.toLowerCase();
        }

        var giveUp = data.giveUp;

        var player = Players.findOne({userUUID:userUUID});

        var round = Rounds.findOne({party: player.party, status: 'started'});
        if (!round) {
            console.log("No round started!");
            throw new Meteor.Error(500, "No round started!", "No round started!");
        }

        round.solvers = round.solvers ? round.solvers : [];

        if (GTW.contains(round.solvers, player._id) ) {
            console.log("Player already solved the wiki");
            throw new Meteor.Error(500, "Player already solved the wiki", "Player already solved the wiki");
        }

        var wiki = Wikis.findOne({player: player._id, round: round._id});

        if (giveUp !== 'true') {

            console.log("Player solution: " + solution);
            console.log("Wiki title: " + wiki.title);


            if (wiki.title == solution) {
                console.log("The solution matches!");
            } else {
                console.log("The solution doesn't match!");
                throw new Meteor.Error(500, "The solution doesn't match!", "The solution doesn't match!");
            }


            var score = player.score ? player.score : 0;
            score += round.playersCount - round.solvers.length;

            console.log("Player score: " + score);

            Players.update(player._id, {$set: {score: score}});        
            Wikis.update(wiki._id, {$set: {status: 'guessed'}} );    
        } else {
            Wikis.update(wiki._id, {$set: {status: 'gaveup'}} );
        }



        Rounds.update(round._id, {$push: {solvers: player._id}} );

        console.log('Remove the wiki');


        return Meteor.call('next', {roundId: round._id});
    };