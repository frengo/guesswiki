methodStart = function(data) {

        console.log("Meteor method --- start");

        var roundId = data.roundId;
        
        var round = Rounds.findOne(roundId);
        if (!round) {
            console.log("No round found!");
            throw new Meteor.Error(500, "No round found!", "No round found!");

        }

        if (round.status !== 'ready') {
            console.log("No round ready!");
            throw new Meteor.Error(500, "No round ready!", "No round ready!");
        }

        // aggiorno il round incrementando il conteggio
        Rounds.update(roundId, {$set: {status: 'started'}});


        return Meteor.call('next', {roundId: roundId});

    };