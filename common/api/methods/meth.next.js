// durata di un round in secondi
var ROUNDLONG = 20;

methodNext = function(data) {
        
        console.log("Meteor method --- next");

        var roundId = data.roundId;
        
        var round = Rounds.findOne(roundId);
        if (!round) {
            console.log("No round found!");
            throw new Meteor.Error(500, "No round found!", "No round found!");

        }

        if (round.status !== 'started') {
            console.log("No round started!");
            throw new Meteor.Error(500, "No round started!", "No round started!");
        }

        // giocatori che hanno dato la soluzione
        var solvers = round.solvers;
        solvers = solvers ? solvers : [];

        // recupero il primo player ordinando per il numero di round fatti e data di inserimento.
        // i giocatori con numero minore di round giocati vengono presi per primi. In caso di round uguali, viene preso il giocatore inserito prima
        var player = Players.findOne(
            {
                party: round.party, 
                _id: { $nin: solvers },
                status: {$ne: "offline"}
            },
            {
                sort: ["roundCount", "date"]
            });

        if (!player) {
            console.log("Player list empty");   
            Rounds.update(roundId, {$set: {status: 'finished'}});
            return {};
        }


        // incremento il numero di round giocati dal player
        player.roundCount = player.roundCount ? ++player.roundCount : 1;

        console.log("Increment round count of the player "+ player.name +" to " + player.roundCount);
        
        // aggiorno il player incrementando il suo round
        Players.update(player._id, {$inc: {roundCount: 1}});


        
        //--------------------------------------
        // controllo da quanto tempo è online il player.. se manca da più di x secondi allora lo posso saltare..
        var now = new Date().getTime();
        var playerLastPresence = player.lastPresence ? player.lastPresence : now;
        if (!player.lastPresence) {
            Players.update(player._id, {$set: {lastPresence: now}});
        }
        var maxSecondsOffline = 10;
        if ((now-playerLastPresence) > maxSecondsOffline*1000) {
            // il player è offline da almeno "maxSecondsOffline"
            console.log("il player " + player.name + " è offline da almeno " + maxSecondsOffline);

            // aggiorno lo stato del player
            Players.update(player._id, {$set: {status: "offline"}});

            Meteor.call('next', {roundId: roundId});
            return;l            
        }
        //--------------------------------------



        var roundStart = new Date().getTime();
        var roundLong = ROUNDLONG; // x secondi

        console.log("Round start "+ roundStart +" - round end " + (roundStart+roundLong*1000));

        // aggiorno il round facendolo puntare al player di turno
        Rounds.update(roundId, {$set: {player: player._id, roundStart: roundStart, roundLong: roundLong}});
        round = Rounds.findOne(roundId);


        if (timers[roundId]) {
            Meteor.clearTimeout(timers[roundId]);
        }

        timers[roundId] = Meteor.setTimeout(function() {

            Meteor.call('next', {roundId: roundId}, function(out) {

                if (out && out.error == 500 && out.reason == 'No round started!') {
                    console.log("Unable to call next cause round finished")
                }
            });
            return;

        }, roundLong*1000);


        console.log("Meteor method end --- next");

        return {
            roundStart: roundStart,
            roundLong: roundLong,
            roundPlayer: player._id
        }


    };