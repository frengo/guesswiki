methodCreate = function(data) {

        console.log("Meteor method --- Start new game");

        console.log("Player name: " + data.playerName);

        var userUUID = Meteor.uuid();
        console.log("user UUID: " + userUUID);
        

        // codice per partecipare al party
        var joinCode = Parties.nextCode();
        console.log("Join code: " + joinCode);

        // inserimento del party che rappresenta una sessione di gioco
        var partyId = Parties.insert({
            joinCode: joinCode, 
            status: 'open', // [open, closed]
            date: new Date() 
        });

        console.log('Party saved. ID: ' + partyId);

        // inserimento del round che rappresenta un giro del gioco
        var roundId = Rounds.insert({
            party: partyId, 
            status: 'pending', // [pending, begun, cancelled, finished]
            date: new Date() 
        });
        console.log('Round saved. ID: ' + roundId);


        var playerId = Players.insertWithDefaults(partyId, data.playerName, true, userUUID);

        console.log('Player saved. ID: ' + playerId);


        return {
            partyId: partyId,
            roundId: roundId,
            playerId: playerId,
            userUUID: userUUID
        };

    }