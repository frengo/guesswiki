methodBegin = function(data) {

        console.log("Meteor method --- begin");

        var partyId = data.partyId;
        var playerId = data.playerId;

        // inizia il gioco
        var party = Parties.findOne(partyId);
        if (!party) {
            console.log("No party found");
            throw new Meteor.Error(500, "No party found", "No party found");
        }

        // cerca un round in stato pending per la partita in corso..
        var round = Rounds.findOne({party: partyId, status: 'pending'});
        if (!round) {

            // inserimento del round che rappresenta un giro del gioco
            var roundId = Rounds.insert({
                party: partyId, 
                status: 'pending', // [pending, begun, cancelled, finished]
                date: new Date() 
            });
            console.log('Round saved. ID: ' + roundId);

            round = Rounds.findOne({party: partyId, status: 'pending'});
        }

        // solo il master può iniziare il gioco
        /*
        var master = Players.findOne({party: party._id, isMaster: true});
        if (!master || !(master._id == playerId) ) {
            console.log("Only the master can begin the game");
            throw new Meteor.Error(500, "Only the master can begin the game", "Only the master can begin the game");
        }
        */

        // controllo che ci siano almeno 2 giocatori
        var players = Players.find({party: party._id}).fetch();
        if (!players) {
            console.log("Two players minimun required");
            throw new Meteor.Error(500, "Two players minimun required", "Two players minimun required");            
        }

        var playersCount = players.length;
        if (playersCount < 2) {
            
            Parties.update(party._id, {$set: {status: 'open', round: round._id}} );         

            console.log("Two players minimun required");
            throw new Meteor.Error(500, "Two players minimun required", "Two players minimun required, party now open!");
        }

        for (var i=0; i<players.length; i++) {
            // aggiorno il numero di round giocati del player, riportandolo a 0. Aggiorno anche l'ultima presenza.. annullandola.
            Players.update(players[i]._id, {$set: {roundCount: 0, status: "online", lastPresence: null}});
            Wikis.retrieveAndInsertWithDefaults(round._id, players[i]._id);
        }


        // aggiorno lo stato del round a started
        Rounds.update(round._id, {$set: {status: 'begun', playersCount: playersCount, solvers: [], gaveups: []}} );

        // aggiorno lo stato del party a closed
        Parties.update(party._id, {$set: {status: 'closed', round: round._id}} );


        return {
            roundId: round._id
        };


    }        
