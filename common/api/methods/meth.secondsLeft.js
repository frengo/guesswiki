methodSecondsLeft = function(data) {
    var round = data.round;
    var userUUID = data.userUUID;

    if(Meteor.isServer) {
        // solo se sono su server
    
        Meteor.setTimeout(function() {

            var player = Players.findOne({userUUID: userUUID});
            if (player) {
                
                // recupero il giocatore con più round giocati
                var pWithMaxRoundPlayed = Players.findOne({party: player.party},{sort: {roundCount:-1}});
//console.log(pWithMaxRoundPlayed);

                var update = {
                    lastPresence: new Date().getTime()
                };

                if (player.status == 'offline') {
                    update.status = 'online';
                    update.roundCount = pWithMaxRoundPlayed.roundCount ? pWithMaxRoundPlayed.roundCount : 1;
                }
                // aggiorno il player forzando lo stato a online, l'ultima presenza ad ora ed il numero di round giocati.
                Players.update(player._id, {$set: update });
            }
        }, 500);
    }

    return Math.round( (round.roundStart + (round.roundLong*1000) - (new Date().getTime())) / 1000);
};