methodApproveWiki = function(data) {
    var wikiId = data.wikiId;
    var playerId = data.playerId;
    // marca come approvata da parte dell'utente

    Wikis.vote(wikiId, playerId, 'approve');
};

methodDisapproveWiki = function(data) {
    var wikiId = data.wikiId;
    var playerId = data.playerId;
    // marca come approvata da parte dell'utente

    Wikis.vote(wikiId, playerId, 'disapprove');
};