methodJoin = function(data) {

        console.log("Meteor method --- Join a game");

        var playerName = data.playerName;
        var joinCode = data.joinCode;

        console.log("join code: " + joinCode);
        joinCode = joinCode;

        var party = Parties.findOne({joinCode: joinCode, status: 'open'});
        console.log(party);

        if (!party) {
            console.log("No party found");
            throw new Meteor.Error(500, "No party found", "No party found");
        }



        var userUUID = Meteor.uuid();
        console.log("user UUID: " + userUUID);


        console.log("Party found, id: " + party._id);

        var round = Rounds.findOne({party: party._id, status: 'pending'});
        console.log('Round saved. ID: ' + round._id);

        var playerId = Players.insertWithDefaults(party._id, playerName, false, userUUID);
        console.log('Player saved. ID: ' + playerId);



        return {
            partyId: party._id,
            roundId: round._id,
            playerId: playerId,
            userUUID: userUUID
        };

    }