methodExit = function(data) {

        console.log("Meteor method --- exit");

        var userUUID = data.userUUID;
        // il giocatore esce dal gioco..
        // rimuovo player e la sua wiki

        console.log("Removing player from the party");

        var player = Players.findOne({userUUID: userUUID});
        if (!player) {
            console.log("No player found");
            throw new Meteor.Error(500, "No player found", "No player found");
        }


        var wikis = Wikis.find({player: player._id});
        wikis.forEach(function(w) {
            console.log("Removing wiki " + w._id);
            Wikis.remove(w._id);    
        });


        if (player.isMaster) {
            console.log("Master is leaving the party.. choose another player as master");
            var playerToPromoveAsMaster = Players.findOne({party: player.party, isMaster: false});

            if (playerToPromoveAsMaster) {
                console.log("Promove another player as master");
                Players.update(playerToPromoveAsMaster._id, {$set: {isMaster: true}});                
            }

        }

        Players.remove(player._id);

        var round = Rounds.findOne({party: player.party, status: {$ne: 'finished'} } );
        if (round && round.status === 'started') {
            Meteor.call('next', {roundId: round._id});    
        }

        

    }